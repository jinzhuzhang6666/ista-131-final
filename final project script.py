import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


'''
'''
df = pd.read_csv("/Users/zhangjinzhu/Desktop/Ista_131/Final project/vgsales.csv")
#df.head()
df = df.dropna()
y = df.groupby(['Year']).sum()
y = y['Global_Sales']
x = y.index.astype(int)
plt.figure(figsize=(12,8))
ax = sns.barplot(y = y, x = x)
ax.set_xlabel(xlabel='Year', fontsize=16)
ax.set_xticklabels(labels = x, fontsize=12, rotation=50)
ax.set_ylabel(ylabel='Sales ($ Millions)', fontsize=16)
ax.set_title(label='Global Game Sales in $ Millions Per Year', fontsize=20)
plt.show()
'''
'''

table = df.pivot_table('Global_Sales', index='Publisher', columns='Year', aggfunc='sum')
publishers = table.idxmax()
sales = table.max()
years = table.columns.astype(int)
#print(years)
data = pd.concat([publishers, sales], axis=1)
data.columns = ['Publisher', 'Global Sales']
#print(data)

plt.figure(figsize=(12,8))
ax = sns.regplot(y='Global Sales', x=years.values, data=data)
ax.set_xlabel(xlabel='Year', fontsize=16)
ax.set_ylabel(ylabel='Sales ($ Millions)', fontsize=16)
ax.set_title(label='Highest Publisher Global Sales Trends', fontsize=20)
#ax.set_xticklabels(labels=years, fontsize=12, rotation=50)
plt.show()

#df.info()
#df['Year'].value_counts().sort_index()
df['sum_sales']=df['Global_Sales'].groupby(df['Year']).cumsum()
df['NA_sum_sales']=df['NA_Sales'].groupby(df['Year']).cumsum()
df['EU_sum_sales']=df['EU_Sales'].groupby(df['Year']).cumsum()
df['JP_sum_sales']=df['JP_Sales'].groupby(df['Year']).cumsum()
df['Other_sum_sales']=df['Other_Sales'].groupby(df['Year']).cumsum()
#df.head(10)
sale_df = df.drop_duplicates(subset = ['Year'],keep = 'last')
#sale_df.head()
sale_df['Year']= sale_df['Year'].astype(int)
#sale_df.head()
sale_df = sale_df.sort_values(by = 'Year', ascending= True)
#sale_df.head()
y_1 = sale_df['sum_sales'].values.tolist()
y_2 = sale_df['NA_sum_sales'].values.tolist()
y_3 = sale_df['EU_sum_sales'].values.tolist()
y_4 = sale_df['JP_sum_sales'].values.tolist()
y_5 = sale_df['Other_sum_sales'].values.tolist()
x_1 = sale_df['Year'].values.tolist()
x = range(len(x_1))
plt.figure(figsize=(12,8))
plt.plot(x,y_1,label = 'Globel')
plt.plot(x,y_2,label = 'North America')
plt.plot(x,y_3,label = 'Europe')
plt.plot(x,y_4,label = 'Japan')
plt.plot(x,y_5,label = 'Other')
xtick_labels = ['{}'.format(i) for i in x_1]
plt.xticks(list(x)[::3],xtick_labels[::3],fontsize = 16)
plt.xlabel('Years',fontsize = 16)
plt.ylabel('Sale ($ Millions)',fontsize = 16)
plt.title('Sales Changing in Different Area',fontsize = 16)
plt.grid(alpha=0.5)
plt.legend(loc = 'upper right')
plt.show()